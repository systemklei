from models import *
from django.contrib import admin

admin.site.register(Person)
admin.site.register(Institution)
admin.site.register(Author)
admin.site.register(Proceeding)
admin.site.register(Paper)
