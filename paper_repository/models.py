from django.db import models
from taggit.managers import TaggableManager

def computeFilePath(instance, filename):
    # TODO: fill in this method
    return filename


class Institution(models.Model):
    name = models.CharField(max_length=100)
    country = models.CharField(max_length=20)

    def __unicode__(self):
        return u'%s (%s)' % (self.name, self.country)


class Person(models.Model):
    name = models.CharField(max_length=100)
    institution = models.ManyToManyField(Institution, through='Author')

    def __unicode__(self):
        return unicode(self.name)


class Author(models.Model):
    person = models.ForeignKey(Person)
    institution = models.ForeignKey(Institution)
    email = models.EmailField(blank=True)

    def __unicode__(self):
        return u'%s - %s' % (self.person, self.institution)


class Proceeding(models.Model):
    title = models.CharField(max_length=200)
    date = models.DateField()
    editor = models.CharField(max_length=100)
    address = models.CharField(max_length= 80, blank=True)
    institution = models.CharField(max_length=80, blank=True)

    def __unicode__(self):
        return unicode(self.title)


class Paper(models.Model):
    title = models.CharField(max_length=200)
    authors = models.ManyToManyField(Author)
    abstract = models.TextField()
    proceeding = models.ForeignKey(Proceeding)
    pages = models.CharField(max_length=10, blank=True)
    keywords = TaggableManager()
    handle = models.CharField(max_length=80, blank=True)
    filename = models.FileField(upload_to=computeFilePath, blank=True)

    def __unicode__(self):
        return u'%s: %s' % (self.title, ", ".join(unicode(x) for x in self.authors.all()))
